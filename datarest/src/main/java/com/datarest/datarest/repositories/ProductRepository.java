package com.datarest.datarest.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.datarest.datarest.entity.Product;

@RepositoryRestResource(collectionResourceRel =  "data" ,path = "product" )
public interface ProductRepository extends MongoRepository<Product, String> {
	
	public List<Product> findByNameContaining(String name);

}
