package com.datarest.datarest.repositories.validators;

import javax.validation.Valid;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.datarest.datarest.entity.Product;

@Component("beforeCreateProductValidator")
public class ProductValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {

		return Product.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Product  product = (Product) target;
		
		if (product.getName() == null || product.getName().isEmpty())
			errors.rejectValue("name", "name.empty");
		
	}

}
