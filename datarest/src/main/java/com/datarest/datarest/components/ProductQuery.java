package com.datarest.datarest.components;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.datarest.datarest.entity.Product;
import com.datarest.datarest.repositories.ProductRepository;

@Component
public class ProductQuery implements GraphQLQueryResolver {

	@Autowired
	private ProductRepository productRepository;
	
    public List<Product> getProducts(final int count) {
    	
        return this.productRepository.findAll().stream().limit(count).collect(Collectors.toList());
    }

    public Optional<Product> getProduct(final String id) {

        return this.productRepository.findById(id);
    }
	
}
